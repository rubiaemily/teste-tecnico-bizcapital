const tempFood = require('./integration');
jest.mock('./integration');


describe('A funcao getFoods', () => {
  describe('Ao ter sucesso:', () => {
    test('Deve retornar uma receita', async() => {
      const recipe = {
              recipe: 'Meat Stock'
          }
      const resp = {
        data: {
        hits: [recipe]
        }
      };
      tempFood.getRecipe.mockResolvedValue(resp);  
    
      const e = await tempFood.getRecipe()
      expect(e.data.hits[0]).toEqual(recipe);
    });
    describe('Ao ter fracasso', () => {
      test('Deve retornar uma menssagem de "recipe not found"', async () => {
        const message = 'recipe not found'
        const err = {
          cod: '404', message: 'recipe not found'
        }
      tempFood.getRecipe.mockResolvedValue(err);  
    
      const e = await tempFood.getRecipe()
      expect(e.message).toEqual(message);
    });
      
    })
  })
})
describe('A funcao getTemperature', () => {
  describe('Ao ter sucesso:', () => {
    test('Deve retornar uma receita', async() => {
      const temperature = 25
      const resp = {
        main: {
        temp: temperature
        }
      };
      tempFood.getWeather.mockResolvedValue(resp);  
    
      const e = await tempFood.getWeather()
      expect(e.main.temp).toEqual(temperature);
    });
    describe('Ao ter fracasso', () => {
      test('Deve retornar uma menssagem de "city not found"', async () => {
        const message = 'city not found'
        const err = { response: {
          data: {
          cod: '404', message: 'city not found'
        }
      }}
      tempFood.getWeather.mockResolvedValue(err);  
    
      const e = await tempFood.getWeather()
      expect(e.response.data.message).toEqual(message);
    });
      
    })
  })
});