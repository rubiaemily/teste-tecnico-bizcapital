const axios = require('axios');
require('dotenv-safe').load();

async function getRecipe(dishType) {
    const apiKey = process.env.API_RECIPE;
    const food = await axios({
      method: 'get',
      url: `https://api.edamam.com/search?q=${dishType}&app_id=401ee37d&app_key=${apiKey}&to=1`,
    });
    if (!food.data.hits[0]) throw new Error({ cod: '404', message: 'recipe not found' });
    return food.data.hits[0].recipe;
}
  
async function getWeather(city) {
  const apiKey = process.env.API_WEATHER;
  try {
    const temp = await axios({
    method: 'get',
    url: `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`,
  });

  return temp.data;
  } catch (error) {
    return error.response.data;
  }
}

module.exports = {
  getWeather,
  getRecipe,
};