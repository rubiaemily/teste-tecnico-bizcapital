const api = require('../integration/integration');

function dishTypeRecipe(weather) {
  if (weather.main.temp >= 30) return 'salad';
  if (weather.main.temp <= 29 && weather.main.temp >= 20) return 'cereals';
  if (weather.main.temp < 20) return 'soup';
}

function errorApi(err) {
  console.log(err);
  if (err.cod === '404') return err;
  if (err.cod === '429') return { cod: err.cod, message: 'api does not respond' };
  return { cod: '500', message: 'there was a problem server' };
}

async function getWeatherAndRecipe(city) {
  try {
    const weather = await api.getWeather(city);
  if (weather.cod !== 200) throw errorApi(weather);
  const dishType = dishTypeRecipe(weather);
    const recipeAccordingWeather = await api.getRecipe(dishType);
    return {
      cod: 200, message:
        [{
          temp:
          {
            temp: weather.main, city: weather.name
          }, recipe: {
            label: recipeAccordingWeather.label, image: recipeAccordingWeather.image, ingredients: recipeAccordingWeather.ingredientLines, calories: recipeAccordingWeather.calories, cuisineType: recipeAccordingWeather.cuisineType, dishType: recipeAccordingWeather.dishType,
          },
        }]
    };
  } catch (error) {
    return error;
  } 
}

module.exports = getWeatherAndRecipe;
