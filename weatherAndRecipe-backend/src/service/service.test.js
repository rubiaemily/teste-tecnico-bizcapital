const tempFood = require('./service');
jest.mock('./service');

describe('A funcao getFoodAccordingTemperature', () => {
  describe('Ao ter sucesso:', () => {
    test('Deve retornar uma receita', async () => {
      const result = [
        {
          "temp": {
            "temp": 27.5,
            "feels_like": 28.83,
            "temp_min": 26.75,
            "temp_max": 31.08,
            "pressure": 1012,
            "humidity": 61
          },
          "recipe": {
            "image": "https://www.edamam.com/web-img/dab/dab7bb01c829b971a4e39754499f4507.jpeg",
            "ingredients": [
              {
                "text": "2.5 pounds beef marrow bones",
                "quantity": 2.5,
                "measure": "pound",
                "food": "beef marrow",
                "weight": 1133.980925,
                "foodCategory": "meats",
                "foodId": "food_bsmpfmbbcpw2v8bmiis8ibuab0i3",
                "image": "https://www.edamam.com/food-img/625/625c3fd1616edc8df52672410a384de6.jpg"
              },
              {
                "text": "1 large onion, quartered",
                "quantity": 1,
                "measure": "<unit>",
                "food": "onion",
                "weight": 150,
                "foodCategory": "vegetables",
                "foodId": "food_bmrvi4ob4binw9a5m7l07amlfcoy",
                "image": "https://www.edamam.com/food-img/205/205e6bf2399b85d34741892ef91cc603.jpg"
              },
              {
                "text": "2 carrots, sliced",
                "quantity": 2,
                "measure": "<unit>",
                "food": "carrots",
                "weight": 122,
                "foodCategory": "vegetables",
                "foodId": "food_ai215e5b85pdh5ajd4aafa3w2zm8",
                "image": "https://www.edamam.com/food-img/121/121e33fce0bb9546ed7d060b6c114e29.jpg"
              },
              {
                "text": "1 leek, cleaned and sliced",
                "quantity": 1,
                "measure": "<unit>",
                "food": "leek",
                "weight": 89,
                "foodCategory": "vegetables",
                "foodId": "food_a27jevnb06c1m9ax7k41xbbcwcuo",
                "image": "https://www.edamam.com/food-img/4ae/4ae9e09d029a28e0e2c64bdfdbf3f6ae.jpg"
              },
              {
                "text": "2 celery stalks, sliced",
                "quantity": 2,
                "measure": "stalk",
                "food": "celery",
                "weight": 80,
                "foodCategory": "vegetables",
                "foodId": "food_bffeoksbyyur8ja4da73ub2xs57g",
                "image": "https://www.edamam.com/food-img/d91/d91d2aed1c36d8fad54c4d7dc58f5a18.jpg"
              },
              {
                "text": "2.5 pounds organic beef stew meat, cubed",
                "quantity": 2.5,
                "measure": "pound",
                "food": "beef stew meat",
                "weight": 1133.980925,
                "foodCategory": "meats",
                "foodId": "food_bknby1la98smrsbwnthinbam42nj",
                "image": "https://www.edamam.com/food-img/bab/bab88ab3ea40d34e4c8ae35d6b30344a.jpg"
              },
              {
                "text": "2 tablespoons tomato paste",
                "quantity": 2,
                "measure": "tablespoon",
                "food": "tomato paste",
                "weight": 32,
                "foodCategory": "canned vegetables",
                "foodId": "food_auu2atfal07b6gbd1a5wsawy7u0s",
                "image": "https://www.edamam.com/food-img/aef/aef4e029118da71388e526086506053a.jpg"
              },
              {
                "text": "5 cloves garlic",
                "quantity": 5,
                "measure": "clove",
                "food": "garlic",
                "weight": 15,
                "foodCategory": "vegetables",
                "foodId": "food_avtcmx6bgjv1jvay6s6stan8dnyp",
                "image": "https://www.edamam.com/food-img/6ee/6ee142951f48aaf94f4312409f8d133d.jpg"
              },
              {
                "text": "2 bay leaves",
                "quantity": 2,
                "measure": "<unit>",
                "food": "bay leaves",
                "weight": 1.2,
                "foodCategory": "Condiments and sauces",
                "foodId": "food_asx39x4ayja4jab6ivj6zayvkblo",
                "image": "https://www.edamam.com/food-img/0f9/0f9f5f95df173e9ffaaff2977bef88f3.jpg"
              },
              {
                "text": "3 sprigs thyme",
                "quantity": 3,
                "measure": "sprig",
                "food": "thyme",
                "weight": 9,
                "foodCategory": "Condiments and sauces",
                "foodId": "food_b3o3cj7a5gskecb0ufphtadnbfqb",
                "image": "https://www.edamam.com/food-img/3e7/3e7cf3c8d767a90b906447f5e74059f7.jpg"
              },
              {
                "text": "3 sprigs Italian parsley",
                "quantity": 3,
                "measure": "sprig",
                "food": "parsley",
                "weight": 3,
                "foodCategory": "vegetables",
                "foodId": "food_b244pqdazw24zobr5vqu2bf0uid8",
                "image": "https://www.edamam.com/food-img/46a/46a132e96626d7989b4d6ed8c91f4da0.jpg"
              },
              {
                "text": "1/2 teaspoon black peppercorns",
                "quantity": 0.5,
                "measure": "teaspoon",
                "food": "black peppercorns",
                "weight": 1.45,
                "foodCategory": "Condiments and sauces",
                "foodId": "food_b6ywzluaaxv02wad7s1r9ag4py89",
                "image": "https://www.edamam.com/food-img/c6e/c6e5c3bd8d3bc15175d9766971a4d1b2.jpg"
              }
            ],
            "calories": 467.40532133000005,
            "cuisineType": [
              "american"
            ],
            "dishType": [
              "soup"
            ]
          }
        }
      ]
      const temperature = 25
      const resp = {
        main: {
        temp: temperature
        }
      };
      tempFood.mockResolvedValue(result);  
    
      const e = await tempFood()
      expect(e).toEqual(result);
    });
    describe('Ao ter fracasso', () => {
      test('Deve retornar uma menssagem de "city not found"', async () => {
        const message = 'city not found'
        const err = { response: {
          data: {
          cod: '404', message: 'city not found'
        }
      }}
      tempFood.mockResolvedValue(err);  
    
      const e = await tempFood()
      expect(e.response.data.message).toEqual(message);
    });
      
    })
  })
});
