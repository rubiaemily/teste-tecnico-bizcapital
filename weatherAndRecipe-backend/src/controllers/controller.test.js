const frisby = require('frisby');
require('dotenv-safe').config();


const url = `http://localhost:${process.env.PORT}`;
describe('Será validado que', () => {
  it('é possível listar uma temperatura e uma receita quando passado a cidade corretamente', async () => {
    await frisby
      .get(`${url}/recipe/?city=sao paulo`)
      .expect('status', 200)
      .then((response) => {
        const { temp, recipe } = response.body[0];
        expect(temp);
        expect(recipe)
      });
  });
  it('nao é possível listar uma temperatura e uma receita quando passado a cidade errada', async () => {
    await frisby
      .get(`${url}/recipe/?city=saopaulo`)
      .expect('status', 404)
      .then((response) => {
        const message = response.body;
        expect(message).toBe("{\"message\":\"city not found\"}");
      });
  });
})
