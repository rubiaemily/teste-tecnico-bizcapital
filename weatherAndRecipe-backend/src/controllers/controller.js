const rescue = require('express-rescue');
const recipeWeather = require('../service/service');

const getWeatherAndRecipe = rescue(async (req, res) => {
  const { city } = req.query;
  const recipeAndTemp = await recipeWeather(city);

    res.status(recipeAndTemp.cod).send(recipeAndTemp.message);


});

module.exports = {
  getWeatherAndRecipe,
};
