const router = require('express').Router();
const controller = require('../controllers/controller');

router.get('/', controller.getWeatherAndRecipe);

module.exports = router;