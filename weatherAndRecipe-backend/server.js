const express = require('express');
require('dotenv-safe').load();
const cors = require('cors');
const morgan = require('morgan');
const router = require('./src/routes/routes');

const app = express();
app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
app.use('/recipe', router);
const port = process.env.PORT;

app.listen(port, () => {
console.log(`Listening to port http://localhost:${port}`);
});