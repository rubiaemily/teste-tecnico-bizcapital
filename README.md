# Boas vindas ao repositório do projeto Climas e Comidas!

---

## Sumário
- [Funcionalidades](#funcionalidades)
- [Requisitos](#requisitos)
- [Tecnologia](#tecnologia)

---

## Funcionalidades:
  
Esse é  um projeto tem o intuito de ao receber o nome de uma cidade e retorne uma receita baseada na temperatura atual

---

## Requisitos

- Se a temperatura estiver acima de 30º C sugira uma salada;

- Se a temperatura estiver entre 20º C e 29º C sugira uma prato com cereais e/ou
carnes;

- Se a temperatura estiver abaixo de 20º C sugira sopas ou/e caldos.


---

## Tecnologia

  ### 1. Front-End:

  - React;

  ### 2. Back-End:

  - NodeJS;

  ### 3. Sistema de Loggin:

  - Morgan
  
  ### 4. APIs:

  - [OpenWeatherMaps](https://openweathermap.org/);

  - [edamam](https://developer.edamam.com/)

---