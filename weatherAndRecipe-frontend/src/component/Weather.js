import React, { useState, useEffect } from 'react';
import { checkTemperaureAndFood } from '../service/weather&recipeApi';
import Loading from './Loading';
import './Recipe.css';



export default function Weather() {
  const [city, setCity] = useState('');
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const localTemp = JSON.parse(localStorage.getItem('dataTemp'));
  
  useEffect(() => {
    if (localTemp) {
      setData(localTemp)
      setLoading(false)
    } else {
      setData('')
      setLoading(false)
    }
  }, []);

  const searchCity = (event) => {
    if (event.key === 'Enter') {
      api(city);
      setLoading(true);
    }
  };

  const api = async (location) => {
    const api = await checkTemperaureAndFood(location);
    if (api.message) {
      setData('there was an error');
      setLoading(false);
    } else {
      setData(api.data[0]);
      localStorage.setItem('dataTemp', JSON.stringify(api.data[0]));
      setLoading(false);
    }
  };

  const tempRecipe = () => {
    return (
      <>
        <div className='conteiner'>
          <div className='top'>
            <div className='location'>
              <p>{data.temp.city}</p>
            </div>
            <div className='temp'>
              <h1>{`${parseInt(data.temp.temp.temp)} °C`}</h1>
            </div>
          </div>
        </div>
        <div className='recipe'>
          <h2>{data.recipe.label}</h2>
          <p>{parseInt(data.recipe.calories)} calories</p>
          <h3 >{data.recipe.dishType}</h3>
          <p></p>
          <ol>
            <h3 className='ingredients'>Ingredients:</h3>
            {data.recipe.ingredients.map(ingredient => (
              <li className='recipes' key={ingredient}>{ingredient}</li>
            ))}
          </ol>
              
  
          <img className='image' src={data.recipe.image} alt="" />
  
        </div>
      </>
    )
  };


  return (
    <>
      <div className='search'>
        <input
          onChange={(e) => setCity(e.target.value)}
          onKeyPress={searchCity}
          placeholder={'Enter City'}
          type='text'
        />
      </div>
       {loading ? <Loading /> : typeof data === "string" ? <h2 className='search'>{data}</h2> :tempRecipe()}
    </>
  )
};
