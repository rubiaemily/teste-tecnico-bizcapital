import { render, screen,fireEvent } from '@testing-library/react';
import Weather from './component/Weather';

test('Ha um PlaceHolder ', () => {
  render(<Weather />);
  const linkElement = screen.findByText(/Enter City/i);
  expect(linkElement).toBeTruthy();
  
});
