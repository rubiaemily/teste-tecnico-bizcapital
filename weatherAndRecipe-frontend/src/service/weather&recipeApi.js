import axios from 'axios';

const API_URL = 'http://localhost:4000/';

export const  checkTemperaureAndFood= async (city) => {
  try {
    const respostas = await axios.get(
      `${API_URL}recipe/?city=${city}`,
    );
    return respostas;
  } catch (err) {
    return err;
  }
};
